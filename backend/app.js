import "dotenv/config";
import express, { json } from "express";
import cors from "cors";
import { getImageKeysByUser, getUserPresignedUrls } from "./index.js";

const app = express();

const PORT = process.env.PORT || 4000;

app.use(
  cors({
    origin: "*",
  })
);
app.use(json());

app.get("/images", async (req, res) => {

  const { error, presignedUrls } = await getUserPresignedUrls();
  if (error) return res.status(400).json({ message: error.message });
  return res.json(presignedUrls);
});

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
