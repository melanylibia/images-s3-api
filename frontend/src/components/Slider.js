import axiosClient from '../config/axios';
import React, { useEffect, useState } from 'react'
import Carousel from 'react-bootstrap/Carousel';
import uuid from 'react-uuid';
import { LazyLoadImage } from 'react-lazy-load-image-component';

import '../App.css'
function Slider() {
    const url = '/images'
    const [images, setImages] = useState([])
    useEffect(() => {

        const fetch = async () => {
            axiosClient
                .get(url)
                .then(res => setImages(res.data))
                .catch(error =>
                    console.log(error)
                );
        };

        fetch();
    }, [url]);

    return (
        <div>
            <div className="container p-4">
                <div className="row">
                    <div className="col-sm carouesl carousel-inner" >
                        <Carousel>
                            {images.map(image =>
                                <Carousel.Item key={uuid()}>
                                    <LazyLoadImage
                                        className="d-block w-100"
                                        src={image}
                                        alt="# slide"

                                    />
                                </Carousel.Item>
                            )}
                        </Carousel>
                    </div>
                </div>
            </div>
        </div>
    )

}


export default Slider